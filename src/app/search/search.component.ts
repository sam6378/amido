import { Component, OnInit } from '@angular/core';
import * as Rx from 'rxjs';

import { ApiService } from '../api.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchTermSubject: Rx.Subject<string> = new Rx.Subject();
  apiResult: any[] = [];

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.searchTermSubject.debounceTime(500).subscribe(searchTextValue => {
      this.handleSearch(searchTextValue);
    });
  }

  onKeyUp(event: any){
    this.searchTermSubject.next(event.target.value);
  }

  handleSearch(term) {
    this.apiService.get(term)
      .subscribe(result => this.apiResult = result.results, console.error);
  }
}
