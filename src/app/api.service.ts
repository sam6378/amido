import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';

import * as Rx from 'rxjs';

import { GLOBALS } from './globals';

@Injectable()
export class ApiService {

    constructor(public http: Http) { }

    get(term: string, limit: number = 10): Rx.Observable<any> {
      return this.http.get(GLOBALS.ITUNES_URL + "?term=" + term + '&limit=' + limit, {})
        .map((res: Response) => res.json())
        .catch(this.handleError);
    }

    private handleError(error: Response | any) {
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      return Promise.reject(errMsg);
    }
}
