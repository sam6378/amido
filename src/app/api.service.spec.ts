import { HttpModule, Http, XHRBackend, Response, ResponseOptions } from '@angular/http';
import { TestBed, inject, async } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { ApiService } from './api.service';

describe('ApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiService],
      imports: [HttpModule],
    });
  });

  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
  }));

  describe('when get', () => {

    let backend: MockBackend;
    let apiService: ApiService;
    let fakeData: any;
    let response: Response;

    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      apiService = new ApiService(http);
      fakeData = {
        "resultCount":3,
        "results": [
          {
            "wrapperType":"track",
            "kind":"song",
            "artistId":159260351,
            "collectionId":907242701,
            "trackId":907242707,
            "artistName":"Taylor Swift Change",
            "collectionName":"1989",
            "trackName":"Shake It Off"
          },
          {
            "wrapperType":"track",
            "kind":"song",
            "artistId":159260351,
            "collectionId":907242701,
            "trackId":907242703,
            "artistName":"Taylor Swift",
            "collectionName":"1989",
            "trackName":"Blank Space"
          },
          {
            "wrapperType":"track",
            "kind":"song",
            "artistId":159260351,
            "collectionId":907242701,
            "trackId":907242710,
            "artistName":"Taylor Swift",
            "collectionName":"1989",
            "trackName":"Wildest Dreams"
          }
        ]
      };
      let options = new ResponseOptions({status: 200, body: fakeData});
      response = new Response(options);
    }));

    it('should have expected fake tracks', async(inject([], () => {
      // backend.connections.subscribe((mockConnection: MockConnection) => mockConnection.mockRespond(response));

      // TODO finish mock backend implementation
      apiService.get('Taylor Swift', 3)
        .subscribe((result: any) => {
          expect(result.results.length).toBe(3, 'should have expected no. of tracks');
        });
    })));
  });
});
