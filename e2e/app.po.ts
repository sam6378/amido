import { browser, element, by } from 'protractor';

export class AmidoPage {
  navigateTo() {
    return browser.get('/');
  }

  getNoResultsText() {
    return element(by.css('app-root .no-results')).getText();
  }

  inputSearch(searchTerm: string) {
    return element(by.css('app-root .input-search')).sendKeys(searchTerm);
  }

  getResults() {
    return element(by.css('app-root .search-results li:first-child'));
  }

  waitForResults() {
    return browser.wait(() => {
      return browser.isElementPresent(by.css('.search-results'));
    }, 50000);
  }
}
