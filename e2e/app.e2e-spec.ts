import { AmidoPage } from './app.po';

describe('amido App', () => {
  let page: AmidoPage;

  beforeEach(() => {
    page = new AmidoPage();
  });

  it('should display message saying "No Results"', () => {
    page.navigateTo();
    expect(page.getNoResultsText()).toEqual('No Results');
  });

  it('should display results', () => {
    page.navigateTo();
    page.inputSearch('Taylor Swift');
    page.waitForResults().then(result => {
      expect(page.getResults()).toBeDefined();
    });
  });
});
